# Evoc-learn

#### This project contains the code to reproduce the results in:

#### Anqi Xu, Daniel R. van Niekerk, Branislav Gerazov, Paul Konstantin Krug, Peter Birkholz, Santitham Prom-on, Lorna F. Halliday, Yi Xu. A computational model for human vocal learning.

#### The workflow of the vocal learning model is illustrated in the following plot:

#### ![alt text](Demo/Figure1d.png "Model overview")

#### [Demonstration video](https://gitlab.com/Anqi_Xu/evoc_learn/-/blob/main/Demo/cvc_cvcv_updated.mp4) introduces the structure of the vocal learning model and shows the English words learned by the adult and the child vocal tract models with 3D articulatory movements.

#### [Stimuli](https://gitlab.com/Anqi_Xu/evoc_learn/-/tree/main/Stimuli) contains the synthetic samples used in the perception experiments.

#### [Code](https://gitlab.com/Anqi_Xu/evoc_learn/-/tree/main/Code) contains the code to reproduce the results, please follow the instructions.

##### Step 1: Installation

Create virtual environment to install the dependencies (recommended):

For Linux & Unix (Mac M1 is not supported at the moment)
```plaintext
python3 -m venv env
source env/bin/activate
pip install evoclearn-rec==0.4.2
```

For Windows
```plaintext
py -m venv env
.\env\Scripts\activate
pip install evoclearn-rec==0.4.2
```
##### Step 2: Set up

Select and copy an initialisation file from [Initialization_files](https://gitlab.com/Anqi_Xu/evoc_learn/-/tree/main/Code/Initialization_files)

Paste it in [Code](https://gitlab.com/Anqi_Xu/evoc_learn/-/tree/main/Code)

*See [Instructions](https://gitlab.com/Anqi_Xu/evoc_learn/-/blob/main/Code/Initialization_files/Instructions.pdf) for further details of the initialization files 

##### Step 3: Run

For Linux & Unix
```plaintext
python3 optimization.py
```

For Windows
```plaintext
py optimization.py
```

The process will generate three synthetic samples with the lowest error. The auditory feedback errors of all the accepted trails will be listed in '_costs.csv'. The learned articulatory parameters of accepted trails will be listed in '_VTP1.csv'(consonant), '_VTP2.csv'(vowel), '_VTP_taus.csv'(time constants of the consonant and the vowel). The learned glottis parameters of accepted trails will be listed in '_GLP1.csv'(consonant), '_GLP2.csv'(vowel), '_GLP_taus.csv'(time constants of the consonant and the vowel). 

##### Step 4: Exit

```plaintext
deactivate
```
