import os, sys, ctypes, random,librosa,multiprocessing,json,glob,ast,platform
import numpy as np
from scipy.io import wavfile
import pandas as pd
from types import SimpleNamespace 
import evoc_utils as utils #evoc_utils.py should be in the same path or installed locally
import acoustic_features as ft
import evoclearn.core
from evoclearn.core import Sequence, Waveform, log
from evoclearn.rec import Ac2Vec
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'#ignore tensorflow warning
pd.options.mode.chained_assignment = None  # default='warn'

def neutral_start(speaker_file):
    """ VTP of a schwa and GLP of a modal voice"""
    VTP = {}
    GLP = {}
    VTP_neutral, VTP_range, GLP_neutral, GLP_range = utils.read_speaker_file(speaker_file)
    VTP["VTP_1"] = VTP_neutral.tolist()
    VTP["VTP_2"] = VTP_neutral.tolist()
    VTP["tau_s_1"] = 0.012
    VTP["tau_s_2"] = 0.012 
    GLP["GLP_1"] = GLP_neutral.tolist()
    GLP["GLP_2"] = GLP_neutral.tolist()
    GLP["tau_s_1"] = 0.012
    GLP["tau_s_2"] = 0.012 
    return VTP, GLP

def clip(x,param_range):
    """ Force x to be in the interval.""" 
    a = param_range[0]
    b = param_range[1]
    return max(min(x, b), a)

def calculate_delta(fraction,param_range):
    """define the step of random neibourgh by pramater range""" 
    amplitude = (param_range[1]-param_range[0])* fraction
    delta = (-amplitude/2.) + amplitude * np.random.random_sample()
    return delta

def acceptance_probability(cost, new_cost, temperature):
    if new_cost < cost:
        #print("    - Acceptance probabilty = 1 as new_cost = {} < cost = {}...".format(new_cost, cost))
        return 1
    else:
        p = np.exp(- (new_cost - cost) / temperature)
        #print("    - Acceptance probabilty = {:.3g}...".format(p))
        return p

def temperature(fraction):
    """ Example of temperature decreasing as the process goes on."""
    return max(0.01, min(1, 1 - fraction))

def GLP_search(speaker_file,GLP,CV1_VTP,syllable_structure, fraction=1, finetuning=False):
    VTP_neutral, VTP_range, GLP_neutral, GLP_range = utils.read_speaker_file(speaker_file)
    if finetuning == False:
        if CV1_VTP != None and syllable_structure == 'CVC': #GLP for coda
            params_1=GLP_neutral.copy()
            params_1[1]=1000
            params_2=GLP_neutral.copy()
            params_2[1]=random.uniform(2000,5000)
            params_2[2]=random.uniform(0,2) 
            params_2[3]=random.uniform(0,2)
            for pp in [4,6]:
                params_2[pp] = random.uniform(GLP_range[pp,1], GLP_range[pp,0])   
            GLP["GLP_1"]=params_1.tolist()
            GLP["GLP_2"]=params_2.tolist()
            GLP["tau_s_1"]=random.uniform(0.005,0.015)
            GLP["tau_s_2"]=random.uniform(0.005,0.015)
        else:#GLP for CV
            GLP["GLP_1"]=utils.random_GLP(speaker_file,False).tolist()
            GLP["GLP_2"]=GLP_neutral.tolist()
            GLP["tau_s_1"]=random.uniform(0.010,0.020)
            GLP["tau_s_2"]=0.012    
    else:
        if CV1_VTP != None and syllable_structure == 'CVC': #GLP for coda
            params_1=GLP_neutral.copy()
            params_2=GLP_neutral.copy()           
            #
            params_1[1] = 1000
            #
            delta = calculate_delta(fraction,[2000,5000])
            params_2[1] = clip((GLP["GLP_2"][1] + delta),[2000,5000])
            delta = calculate_delta(fraction,[0,2])
            params_2[2] = clip((GLP["GLP_2"][2] + delta),[0,2])
            delta = calculate_delta(fraction,[0,2])
            params_2[3] = clip((GLP["GLP_2"][2] + delta),[0,2])
            #
            for vv in [4,6]:
                delta = calculate_delta(fraction,GLP_range[vv])
                params_2[vv] = clip((GLP["GLP_2"][vv] + delta),GLP_range[vv])
            #
            delta = calculate_delta(fraction, [0.005,0.015])
            GLP["tau_s_1"]=clip(GLP["tau_s_1"]+delta,[0.005,0.015])
            delta = calculate_delta(fraction, [0.005,0.015])
            GLP["tau_s_2"]=clip(GLP["tau_s_2"]+delta,[0.005,0.015])
        else:#GLP for CV
            params_1=GLP_neutral.copy()          
            for vv in [2,3,4,6]:
                delta = calculate_delta(fraction,GLP_range[vv])
                params_1[vv] = clip((GLP["GLP_1"][vv] + delta), GLP_range[vv])
            GLP["GLP_1"]=params_1.tolist()
            GLP["GLP_2"]=GLP_neutral.tolist()
            #
            delta = calculate_delta(fraction, [0.010,0.020])
            GLP["tau_s_1"]=clip(GLP["tau_s_1"]+delta,[0.010,0.020])
            GLP["tau_s_2"]=0.012 
    return GLP


def VTP_search(VTLlib,speaker_file, VTP, consonant_dimension, n_close, CV1_VTP,
               syllable_structure, fraction=1, finetuning=False):
    """Move a little bit x, from the left or the right."""
    VTP_neutral, VTP_range, GLP_neutral, GLP_range = utils.read_speaker_file(speaker_file)
    if finetuning == False:
        if CV1_VTP != None and syllable_structure == 'CVC': #VTP for coda
            params_2 = VTP_neutral
            while True:
                params_1 = utils.randomVTP_C(consonant_dimension,params_2,VTP_range)
                if utils.consonant_check(VTLlib,speaker_file,params_1,n_close)==1:
                    break
            time_constant_1=random.uniform(0.000,0.010)
            time_constant_2=random.uniform(0.000,0.010)
        else:
            while True:
                params_2 = np.zeros(len(VTP_neutral))# VTP of a vowel
                for pp in range(len(VTP_neutral)):
                    params_2[pp] = random.uniform(VTP_range[pp,1], VTP_range[pp,0])
                if utils.vowel_check(VTLlib,speaker_file,params_2):
                    break
            while True:
                params_1 = utils.randomVTP_C(consonant_dimension,params_2,VTP_range) # VTP of a consonant
                if utils.consonant_check(VTLlib,speaker_file,params_1,n_close):
                    break
            time_constant_1=random.uniform(0.010,0.020)
            time_constant_2=random.uniform(0.010,0.020)
        VTP["VTP_1"] = params_1.tolist()
        VTP["VTP_2"] = params_2.tolist()
        VTP["tau_s_1"] = time_constant_1
        VTP["tau_s_2"] = time_constant_2
    else:
        #
        if CV1_VTP != None and syllable_structure == 'CVC': #VTP for coda
            params_2 = VTP_neutral
            params_1 = np.copy(params_2)
            #
            for cdi in consonant_dimension:
                delta = calculate_delta(fraction,VTP_range[cdi])
                params_1[cdi] = clip((VTP["VTP_1"][cdi] + delta), VTP_range[cdi])
            #   
            delta = calculate_delta(fraction, [0.00, 0.010])
            time_constant_1 = clip(VTP["tau_s_1"]+delta,[0.00, 0.010])
            delta = calculate_delta(fraction, [0.00, 0.010])
            time_constant_2 = clip(VTP["tau_s_2"]+delta,[0.00, 0.010])

        else:
            counter=0
            params_2 = np.zeros(len(VTP_neutral))
            while counter<50:
                for vv in range(len(VTP_neutral)):
                    delta = calculate_delta(fraction,VTP_range[vv])
                    params_2[vv] = clip((VTP["VTP_2"][vv] + delta), VTP_range[vv])
                if utils.vowel_check(VTLlib,speaker_file,params_2):
                    break
                counter+=1
                #print('params2_checked')
            counter=0
            params_1 = np.copy(params_2)
            while counter<50:
                for cdi in consonant_dimension:
                    delta = calculate_delta(fraction,VTP_range[cdi])
                    params_1[cdi] = clip((VTP["VTP_1"][cdi] + delta), VTP_range[cdi])
                if utils.consonant_check(VTLlib,speaker_file,params_1,n_close):
                    break
                counter+=1
                #print('params1_checked')
            delta = calculate_delta(fraction, [0.010, 0.020])
            time_constant_1 = clip(VTP["tau_s_1"]+delta,[0.010, 0.020])
            delta = calculate_delta(fraction, [0.010, 0.020])
            time_constant_2 = clip(VTP["tau_s_2"]+delta,[0.010, 0.020])
        #
        VTP["VTP_1"] = params_1.tolist()
        VTP["VTP_2"] = params_2.tolist()
        VTP["tau_s_1"] = time_constant_1
        VTP["tau_s_2"] = time_constant_2
    return VTP

def get_acoustic_features(sound_file, duration_array=[1], total_duration_array=[1]):
    ratio=sum(duration_array)/sum(total_duration_array)
    if isinstance(sound_file, str): #natural speech
        y, sr =librosa.load(sound_file, sr= 44100)
        y = y[0:int(round(len(y)*ratio))]
    else:
        y = sound_file #synthetic speech
    MFCC, logmel =ft.get_features(y)
    return MFCC

def get_rec(sound_file,duration_array=[1],total_duration_array=[1]):
    ratio=sum(duration_array)/sum(total_duration_array)
    logger = log.getLogger("evl.rec.ac2vec")
    ac2vec = Ac2Vec.from_file("cvc_clean_nopre_26mel_deep_cnn_bilstm_mi9_norm.ac2vec.json.bz2",
                              logger)
    if isinstance(sound_file, str):
        data=pd.read_csv( 'rec_index.csv',delimiter=',')
        data.set_index('target', inplace = True)
        index_list = [ int(i) for i in data.loc[sound_file].tolist() if str(i) != 'nan']
        rec=np.zeros(35)
        for ii in index_list:
            rec[ii] = 1
        if ratio != 1:
            rec=rec[:28]
    else:
        y = sound_file
        rec = list(ac2vec([Waveform(y,44100)], from_wav=True))[0]
        if ratio != 1:
            rec=rec[:28]
    return rec
"""
    ratio=sum(duration_array)/sum(total_duration_array)
    if isinstance(sound_file, str):
        y, sr =librosa.load(sound_file, sr= 44100)
        y = y[0:int(round(len(y)*ratio))]
"""


def analysis_by_synthesis(feedback,VTLlib,speaker_file,VTP,GLP,F0_targets,template,
                          consonant_dimension,CV1_VTP,CV1_GLP):
    #initial settings
    frame_rate = 220
    enableConsoleOutput=int(1)
    #VTL  initialization
    VTL = ctypes.cdll.LoadLibrary(VTLlib)
    #Loading speaker file is necessary for extracting the constant
    speaker_file_name = ctypes.c_char_p(speaker_file.encode())
    VTL.vtlInitialize(speaker_file_name)#VTL.vtlInitialize function is for loading the speaker file
    #get constant form VTL
    audio_sampling_rate = ctypes.c_int(0)
    number_tube_sections = ctypes.c_int(0)
    number_vocal_tract_parameters = ctypes.c_int(0)
    number_glottis_parameters = ctypes.c_int(0)

    VTL.vtlGetConstants(ctypes.byref(audio_sampling_rate),
                                    ctypes.byref(number_tube_sections),
                                    ctypes.byref(number_vocal_tract_parameters),
                                    ctypes.byref(number_glottis_parameters))        
    #initilaize sum of squares
    sum_of_squares =0
    target_sound = './Natural_speech/' +template + '.wav'
    target_textgrid = './Natural_speech/' + template + '.TextGrid'
    #the child's vocal tract model has different VTP from the adult's
    #duration specification for CV1/CV2
    total_duration_array = utils.read_duration_from_TextGrid(target_textgrid)
    duration_array = []
    number_of_param = 0
    if CV1_VTP == None:
        duration_array = total_duration_array[:2]
        number_of_param = 2
    else:
        duration_array = total_duration_array
        number_of_param = 4
    #caculate total duration and creat array for storing the parameter curve
    duration = sum(duration_array)
    number_frames = int(duration * frame_rate)
    #target audio
    if feedback == 'recogniser':
        target = get_rec(template, duration_array, total_duration_array)
    else:
        target = get_acoustic_features(target_sound, duration_array, total_duration_array)
    #Initialize VocalTractLab library
    audio = (ctypes.c_double * int((number_frames-1) * frame_rate))()# new!!!
    enableConsoleOutput = int(1)
    #create empty arrays
    tract_params = (ctypes.c_double * (number_frames * number_vocal_tract_parameters.value))()
    glottis_params = (ctypes.c_double * (number_frames * number_glottis_parameters.value))()
    #initial parameter dictionary
    parameter_dicts = pd.DataFrame(np.zeros((4, 4)), columns='dVal slope duration_s tau_s' .split())
    #Calulate parameter curve
    #VTP targets
    tract_params_list = np.zeros((number_vocal_tract_parameters.value,number_frames))
    #assign duration
    for aa in range(number_of_param):
        parameter_dicts["duration_s"][aa] = duration_array[aa]
    #assign time constants
    if number_of_param == 4:
        parameter_dicts["tau_s"] = [CV1_VTP["tau_s_1"],CV1_VTP["tau_s_2"],
                                        VTP["tau_s_1"],VTP["tau_s_2"]]
    else:
        parameter_dicts["tau_s"][0] = VTP["tau_s_1"]
        parameter_dicts["tau_s"][1] = VTP["tau_s_2"]
    #assign target value 
    for tt in range(number_vocal_tract_parameters.value):
        if number_of_param == 4:
            parameter_dicts["dVal"] = [CV1_VTP["VTP_1"][tt],CV1_VTP["VTP_2"][tt],
                                           VTP["VTP_1"][tt],VTP["VTP_2"][tt]]
        else:
            parameter_dicts["dVal"][0] = VTP["VTP_1"][tt]
            parameter_dicts["dVal"][1] = VTP["VTP_2"][tt]
        paramCurve = utils.calcParamCurve(parameter_dicts)
        tract_params_list[tt,:] = paramCurve[0:(number_frames)]
    #GLP targets
    glottis_params_list = np.zeros((number_glottis_parameters.value,number_frames))
    for gg in range(number_glottis_parameters.value):
        if gg == 0:#F0 target (slope included)
            parameter_dicts["dVal"] = F0_targets["dVal"]
            parameter_dicts["tau_s"] = F0_targets["tau_s"]
            parameter_dicts["slope"] = F0_targets["slope"]
        else: #no slope, default time constant
            if number_of_param == 4:
                parameter_dicts["dVal"] = [CV1_GLP["GLP_1"][gg],CV1_GLP["GLP_2"][gg],
                                               GLP["GLP_1"][gg],GLP["GLP_2"][gg]]
                parameter_dicts["tau_s"] = [CV1_GLP["tau_s_1"],CV1_GLP["tau_s_2"],
                                                GLP["tau_s_1"],GLP["tau_s_2"]]
            else:
                parameter_dicts["dVal"][0] = GLP["GLP_1"][gg]
                parameter_dicts["dVal"][1] = GLP["GLP_2"][gg]
                parameter_dicts["tau_s"][0] = GLP["tau_s_1"]
                parameter_dicts["tau_s"][1] = GLP["tau_s_2"]
            parameter_dicts["slope"] = 0.000
        paramCurve = utils.calcParamCurve(parameter_dicts)
        glottis_params_list[gg,:] = paramCurve[0:(number_frames)]
    # fade in
    glottis_params_list[1,0] = 0
    glottis_params_list[1,1] = 2000
    glottis_params_list[1,2] = 4000
    # fade out
    fade_lungpre=glottis_params_list[1,-10]
    glottis_params_list[1,-10:]= np.repeat(fade_lungpre,10) - np.arange(1,11,1)*fade_lungpre/5
    #pass values to the C array
    tract_params[:] = tract_params_list.ravel('F')
    glottis_params[:] = glottis_params_list.ravel('F')

    #pass the values to the synthesis function  
    VTL.vtlSynthBlock(ctypes.byref(tract_params),  # input
                          ctypes.byref(glottis_params),  # input
                          number_frames,  # input
                          frame_rate,  # input
                          ctypes.byref(audio),  # output
                          enableConsoleOutput)  # output
    #save audio array
    wav = np.array(audio)#convert audio list from VTL to numpy array
    wav = wav[0:int(round(44100*duration))]
    if feedback == 'recogniser':
        synth=  get_rec(wav,duration_array,total_duration_array)
        sum_of_squares=np.sum((target-synth)**2)
    else:
        synth=  get_acoustic_features(wav,duration_array,total_duration_array)
        sum_of_squares= ft.get_error(target,synth)
    return sum_of_squares, wav

def simulated_annealing(maxsteps,initialization_file,finetuning,best_VTP=None, best_GLP=None):
    #load initialisation file
    with open(initialization_file, 'r') as f:
        initialization = json.load(f)
    params = SimpleNamespace(**initialization)
    #define process name
    process_name = multiprocessing.current_process().name
    #find VocalTractLabApi
    if platform.system() == "Windows": 
        VTLlib = "./VocalTractLabApi.dll"
    else: 
        VTLlib = os.path.join(evoclearn.core.__path__[0],"VocalTractLabApi.so")
    if finetuning == False:
        VTP_state,GLP_state = neutral_start(params.speaker_file)
    else:
        VTP_state,GLP_state = best_VTP, best_GLP
    cost, wav = analysis_by_synthesis(params.feedback,VTLlib,params.speaker_file,VTP_state,GLP_state,
                                                                params.F0_targets,params.template,
                                                                params.consonant_dimension,
                                                                params.CV1_VTP,params.CV1_GLP)
    initial_cost = cost
    VTP_states, GLP_states, costs = [VTP_state], [GLP_state], [cost]
    for step in range(maxsteps):
        fraction = step / float(maxsteps)
        T = temperature(fraction)
        new_VTP_state = VTP_search(VTLlib,params.speaker_file, VTP_state,
                                                               params.consonant_dimension, params.n_close,
                                                               params.CV1_VTP, params.syllable_structure,
                                                                fraction, finetuning)
        new_GLP_state = GLP_search(params.speaker_file,GLP_state,params.CV1_VTP,
                                                               params.syllable_structure, fraction, finetuning)
        new_cost, new_wav= analysis_by_synthesis(params.feedback,VTLlib,params.speaker_file,new_VTP_state,
                                                                                      new_GLP_state,params.F0_targets,
                                                                                      params.template,params.consonant_dimension,
                                                                                      params.CV1_VTP,params.CV1_GLP)
        print(f"Step:{step} : T = {T}, cost = {cost},new_cost = {new_cost}")
        #if debug: print(f"Step:{step} : T = {T}, cost = {cost},new_cost = {new_cost}")
        if acceptance_probability(cost/initial_cost, new_cost/initial_cost, T) > random.random():
            VTP_state, GLP_state, cost = new_VTP_state, new_GLP_state, new_cost
            VTP_states.append(VTP_state.copy())
            GLP_states.append(GLP_state.copy())
            costs.append(cost)
            print("  ==> Accept it!")
            print(step)
        else:
            print("  ==> Reject it...")
    #synthesise the top three trials
    best_trials_index = np.argsort(costs)[:3]
    for bb in best_trials_index:
        VTP = VTP_states[bb]
        GLP = GLP_states[bb]
        MSE, wav = analysis_by_synthesis(params.feedback,VTLlib,params.speaker_file,VTP,GLP,
                                                                      params.F0_targets,params.template,
                                                                       params.consonant_dimension,
                                                                      params.CV1_VTP,params.CV1_GLP)
        wav = utils.wav_normalise(wav, level=-1)
        utils.wav_write(wav, f'{params.template}_{process_name}_{round(MSE,8)}.wav', 44100)
    #write down the file
    VTP_index = ['HX','HY', 'JX','JA','LP','LD','VS','VO','TCX','TCY',
                             'TTX','TTY','TBX','TBY','TRX','TRY','TS1','TS2','TS3']
    GLP_index = ['f0','pressure','x_bottom','x_top','chink_area','lag','rel_amp',
                             'double_pulsing','pulse_skewness','flutter','aspiration_strength']
    VTP1_df = pd.DataFrame([key['VTP_1'] for key in VTP_states], columns = VTP_index)
    VTP1_df.to_csv(f"{process_name}_VTP1.csv")
    VTP2_df = pd.DataFrame([key['VTP_2'] for key in VTP_states], columns = VTP_index)
    VTP2_df.to_csv(f"{process_name}_VTP2.csv")
    VTP_taus_df = pd.DataFrame({"tau_s_1":[key['tau_s_1'] for key in VTP_states],
                                "tau_s_2":[key['tau_s_2'] for key in VTP_states]})
    VTP_taus_df.to_csv(f"{process_name}_VTP_taus.csv")
    GLP1_df = pd.DataFrame([key['GLP_1'] for key in GLP_states], columns = GLP_index)
    GLP1_df.to_csv(f"{process_name}_GLP1.csv")
    GLP2_df = pd.DataFrame([key['GLP_2'] for key in GLP_states], columns = GLP_index)
    GLP2_df.to_csv(f"{process_name}_GLP2.csv")
    GLP_taus_df = pd.DataFrame({"tau_s_1":[key['tau_s_1'] for key in GLP_states],
                                "tau_s_2":[key['tau_s_2'] for key in GLP_states]})
    GLP_taus_df.to_csv(f"{process_name}_GLP_taus.csv")
    costs_df = pd.DataFrame({"MSE":costs})
    costs_df.to_csv(f"{process_name}_costs.csv")
    return VTP_states, GLP_states, costs
