import glob
import simulated_annealing as SA
maxsteps = 2000#number of iterations in each random search process
#The paper uses 2000 iterations with 10 processes in parallel
initialization_file = glob.glob("*initialization.json")[0]
SA.simulated_annealing(maxsteps,initialization_file,
                        finetuning=False,best_VTP=None, best_GLP=None)
#start multiple processes
#n_processor=2000#number of sub process
#maxsteps_list = np.repeat(maxsteps, n_processor)
#initialization_file_list = np.repeat(initialization_file, n_processor)
#finetuning_list = np.repeat(False, n_processor)
#args = zip(maxsteps_list,initialization_file_list,finetuning_list)
#pool = multiprocessing.Pool(processes=n_processor)
#pool.starmap(simulated_annealing, args)
#pool.close()
#pool.join()
