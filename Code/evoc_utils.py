""" Collection of utils for evoc-learn.

Authors:
    Anqi Xu, Nov 2019
    Daniel van Niekerk, Nov 2019
    Branislav Gerazov, Nov 2019
"""
import os
import shutil
import ctypes
import random
import xml.etree.ElementTree as et
from collections import namedtuple
import time

import numpy as np
import pandas as pd
import librosa
from praatio import textgrid as tgio
from scipy.io import wavfile
import pickle

def read_duration_from_TextGrid(TextGridfile):
    """read duration array function
    """
    tg = tgio.openTextgrid(TextGridfile, includeEmptyIntervals=True)
    entryList = tg.tierDict["1"].entryList
    number_of_interval = len(entryList)
    duration_array = np.zeros(int(number_of_interval))
    for ww in range(int(number_of_interval)):
        duration_array[ww] = entryList[ww].end - entryList[ww].start
    return duration_array

def read_speaker_file(speaker_file_name):
    """read speaker file function
    """
    VTP_neutral = np.zeros(19)
    VTP_range = np.zeros([19, 2])
    tree = et.parse(speaker_file_name)
    for ee in range(10, 29):  # 24 vocal tract parameters
        VTP_neutral[ee - 10] = float((list(list(list(
            tree.getroot())[0])[0])[ee]).attrib['neutral'])
        VTP_range[ee - 10, 0] = float((list(list(list(
    		   tree.getroot())[0])[0])[ee]).attrib['min'])
        VTP_range[ee - 10, 1] = float((list(list(list(
    		   tree.getroot())[0])[0])[ee]).attrib['max'])

    # glottis model defaults
    GLP_neutral = []
    GLP_range = []
    for model in list(list(tree.getroot())[1]): # glottises
        if int(model.attrib['selected']) == 1:
            #print(model.attrib['type'], 'selected.')
            for control_param in list(list(model)[1]):
                GLP_neutral.append(control_param.attrib['default'])
                GLP_range.append(
                    [control_param.attrib['min'],
                     control_param.attrib['max'],
                     ]
                    )
    GLP_neutral = np.array(GLP_neutral, dtype='float')
    GLP_range = np.array(GLP_range, dtype='float')

    return VTP_neutral, VTP_range, GLP_neutral, GLP_range

def voice_check(VTLlib, speaker_file_name, params_V):
    check = 0 #initial settings
    VTL = ctypes.cdll.LoadLibrary(VTLlib)
    speaker_file_name = ctypes.c_char_p(speaker_file_name.encode())
    VTL.vtlInitialize(speaker_file_name)
    # get some constants
    audio_sampling_rate = ctypes.c_int(0)
    number_tube_sections = ctypes.c_int(0)
    number_vocal_tract_parameters = ctypes.c_int(0)
    number_glottis_parameters = ctypes.c_int(0)
    #
    VTL.vtlGetConstants(ctypes.byref(audio_sampling_rate),
                    ctypes.byref(number_tube_sections),
                    ctypes.byref(number_vocal_tract_parameters),
                    ctypes.byref(number_glottis_parameters))
    #
    TRACT_PARAM_TYPE = ctypes.c_double * number_vocal_tract_parameters.value
    params = TRACT_PARAM_TYPE()
    for jj in range(number_vocal_tract_parameters.value):
            params[jj] = params_V[jj]
    NUM_SPECTRUM_SAMPLES = 512
    SPECTRUM_TYPE = ctypes.c_double * NUM_SPECTRUM_SAMPLES
    magnitude_spectrum = SPECTRUM_TYPE()
    phase_spectrum = SPECTRUM_TYPE()  # in radiants

    VTL.vtlGetTransferFunction(ctypes.byref(params),  # input
                           NUM_SPECTRUM_SAMPLES,  # input
                           ctypes.byref(magnitude_spectrum),  # output
                           ctypes.byref(phase_spectrum))  # output

    max_volume = max(magnitude_spectrum)
    #the thread for the volume velocity transfer function can be modified
    if max_volume>10**0.5:
        check = 1 #'voiced'
    else:
        check = 0 #'voiceless'
    return check

def vowel_check(VTLlib, speaker_file_name, params_V):
    check = 0 #initial settings
    VTL = ctypes.cdll.LoadLibrary(VTLlib)
    speaker_file_name = ctypes.c_char_p(speaker_file_name.encode())
    VTL.vtlInitialize(speaker_file_name)
    # get some constants
    audio_sampling_rate = ctypes.c_int(0)
    number_tube_sections = ctypes.c_int(0)
    number_vocal_tract_parameters = ctypes.c_int(0)
    number_glottis_parameters = ctypes.c_int(0)
    #
    VTL.vtlGetConstants(ctypes.byref(audio_sampling_rate),
                    ctypes.byref(number_tube_sections),
                    ctypes.byref(number_vocal_tract_parameters),
                    ctypes.byref(number_glottis_parameters))
    
    TRACT_PARAM_TYPE = ctypes.c_double * number_vocal_tract_parameters.value
    params = TRACT_PARAM_TYPE()
    for jj in range(number_vocal_tract_parameters.value):
        params[jj] = params_V[jj]

    TUBE_TYPE = ctypes.c_double * number_tube_sections.value
    tubeLength_cm = TUBE_TYPE()
    tubeArea_cm2 = TUBE_TYPE()
    tubeArticulator = (ctypes.c_int * number_tube_sections.value)()
    incisorPos_cm = ctypes.c_int(0)
    tongueTipSideElevation =  ctypes.c_int(0)
    velumOpening_cm2 =  ctypes.c_int(0)

    VTL.vtlTractToTube(ctypes.byref(params), 
   ctypes.byref(tubeLength_cm),  ctypes.byref(tubeArea_cm2),  ctypes.byref(tubeArticulator),
   ctypes.byref(incisorPos_cm), ctypes.byref(tongueTipSideElevation), ctypes.byref(velumOpening_cm2));

    if sum(np.array(tubeArea_cm2) > 0.25) < number_tube_sections.value:
        check = 0
    else:
        check = 1
    return check

def consonant_check(VTLlib, speaker_file_name, params_V,n_close):
    check = 0 #initial settings
    VTL = ctypes.cdll.LoadLibrary(VTLlib)
    speaker_file_name = ctypes.c_char_p(speaker_file_name.encode())
    VTL.vtlInitialize(speaker_file_name)
    # get some constants
    audio_sampling_rate = ctypes.c_int(0)
    number_tube_sections = ctypes.c_int(0)
    number_vocal_tract_parameters = ctypes.c_int(0)
    number_glottis_parameters = ctypes.c_int(0)
    #
    VTL.vtlGetConstants(ctypes.byref(audio_sampling_rate),
                    ctypes.byref(number_tube_sections),
                    ctypes.byref(number_vocal_tract_parameters),
                    ctypes.byref(number_glottis_parameters))
    
    TRACT_PARAM_TYPE = ctypes.c_double * number_vocal_tract_parameters.value
    params = TRACT_PARAM_TYPE()
    for jj in range(number_vocal_tract_parameters.value):
        params[jj] = params_V[jj]

    TUBE_TYPE = ctypes.c_double * number_tube_sections.value
    tubeLength_cm = TUBE_TYPE()
    tubeArea_cm2 = TUBE_TYPE()
    tubeArticulator = (ctypes.c_int * number_tube_sections.value)()
    incisorPos_cm = ctypes.c_int(0)
    tongueTipSideElevation =  ctypes.c_int(0)
    velumOpening_cm2 =  ctypes.c_int(0)

    VTL.vtlTractToTube(ctypes.byref(params), 
   ctypes.byref(tubeLength_cm),  ctypes.byref(tubeArea_cm2),  ctypes.byref(tubeArticulator),
   ctypes.byref(incisorPos_cm), ctypes.byref(tongueTipSideElevation), ctypes.byref(velumOpening_cm2));

    if sum(np.array(tubeArea_cm2) <=0.0001) < n_close and sum(np.array(tubeArea_cm2) <=0.0001) > 0:
        check = 1
    else:
        check = 0
    return check

def constrained_parameter_probability_link(
        parameter_index,
        linked_parameter,
        current_VTP,
        VTP_range,
        ):
    """tongue and velum constraints function
    Constraints1: Tongue parameters constraints: whenever the tongue blade
    paramters (TBX/TBY) were adjusted, those of tongue tip and tongue body
    were also modified by 20% with 1% resistance
    """
    # 	VO 7 changes according to TCY (index number: 9)
    if parameter_index == 7:
        direction = current_VTP[9] - linked_parameter
    # TCX8 and TTX10 change according to TBX (index number: 12)
    elif parameter_index in [8, 10]:
        direction = linked_parameter - current_VTP[12]
    # TCY9 and TTY11 change according to TBY (index number: 13)
    else:
        direction = linked_parameter - current_VTP[13]
    CoinToss = random.randint(1, 5)
    # 20% of moving in the opposite direction
    if CoinToss == 1:
        if direction < 0:
            # if the current number is the maximum value, stay as the maximum
            if current_VTP[parameter_index] >= VTP_range[parameter_index, 1]:
                constrained_parameter = VTP_range[parameter_index, 1]
            # generate a random number between the current parameter and max
            else:
                constrained_parameter = random.uniform(
                    current_VTP[parameter_index],
                    VTP_range[parameter_index, 1]
                    )
        else:
            # if the current number is the minimum value, stay as the mimimum
            if current_VTP[parameter_index] <= VTP_range[parameter_index, 0]:
                constrained_parameter = VTP_range[parameter_index, 0]
            # generate a random number between the current parameter and min
            else:
                constrained_parameter = random.uniform(
                    VTP_range[parameter_index, 0],
                    current_VTP[parameter_index]
                    )
    else:
        if direction > 0:
            # if the current number is the maximum value, stay as the maximum
            if current_VTP[parameter_index] >= VTP_range[parameter_index, 1]:
                constrained_parameter = VTP_range[parameter_index, 1]
            # generate a random number between the current parameter and max
            else:
                constrained_parameter = random.uniform(
                    current_VTP[parameter_index],
                    VTP_range[parameter_index, 1]
                    )
        else:
            # if the current number is the minimum value, stay as the mimimum
            if current_VTP[parameter_index] <= VTP_range[parameter_index, 0]:
                constrained_parameter = VTP_range[parameter_index, 0]
            # generate a random number between the current parameter and min
            else:
                constrained_parameter = random.uniform(
                    VTP_range[parameter_index, 0],
                    current_VTP[parameter_index]
                )
    return constrained_parameter

def random_GLP(speaker_file,coda=False):
    VTP_neutral, VTP_range, GLP_neutral, GLP_range = read_speaker_file(speaker_file)
    if coda==False:
        for pp in [2,3,4,6]:
            GLP_neutral[pp] = random.uniform(GLP_range[pp,1], GLP_range[pp,0])    
    else:#GLP for coda
        GLP_neutral[1]=random.uniform(1000,8000)
        for pp in [2,3,4,6]:
            GLP_neutral[pp] = random.uniform(GLP_range[pp,1], GLP_range[pp,0])    
    return GLP_neutral

def randomVTP(
        current_VTP,
        VTP_range
        ):
    """Generate random vocal tract parameters function
    """
    # Generate random number from 0 to 1
    HX = random.uniform(VTP_range[0, 0], VTP_range[0, 1])
    HY = random.uniform(VTP_range[1, 0], VTP_range[1, 1])
    JX = random.uniform(VTP_range[2, 0], VTP_range[2, 1])
    JA = random.uniform(VTP_range[3, 0], VTP_range[3, 1])
    LP = random.uniform(VTP_range[4, 0], VTP_range[4, 1])
    LD = random.uniform(VTP_range[5, 0], VTP_range[5, 1])
    VS = random.uniform(VTP_range[6, 0], VTP_range[6, 1])
    VO = random.uniform(VTP_range[7, 0], VTP_range[7, 1])
    TBX = random.uniform(VTP_range[12, 0], VTP_range[12, 1])
    TBY = random.uniform(VTP_range[13, 0], VTP_range[13, 1])
    TS1 = 0.000
    TS2 = 0.000
    TS3 = -1.000
    # Get VTP of tongue body (TCX, TCY), tongue tip (TTX,TTY) and
    # velum opening (VO) by constraints
    TCX = constrained_parameter_probability_link(
        8, TBX, current_VTP, VTP_range
        )
    TCY = constrained_parameter_probability_link(
        9, TBY, current_VTP, VTP_range
        )
    TTX = constrained_parameter_probability_link(
        10, TBX, current_VTP, VTP_range
        )
    TTY = constrained_parameter_probability_link(
        11, TBY, current_VTP, VTP_range
        )

    hypothetical_TRY = (HY + TCY)/2
    # if the average position is within the range, just use the average
    if ((hypothetical_TRY <= VTP_range[15, 1])
            and (hypothetical_TRY >= VTP_range[15, 0])):
        TRY = hypothetical_TRY
    # if it is closer to the maximum stay as the maximum
    elif (abs(hypothetical_TRY-VTP_range[15, 1])
          - abs(hypothetical_TRY-VTP_range[15, 0])
          ) > 0:
        TRY = VTP_range[15, 1]
    # stay as the minimum
    else:
        TRY = VTP_range[15, 0]

    # average can be higher than max
    TRX_right_boundary = np.min([(HX + TCX)/2, VTP_range[14, 1]])
    # right boundary is higher than the maximum value
    if TRX_right_boundary <= VTP_range[14, 0]:
        TRX = VTP_range[14, 0]
    # right boundary is higher than the minimum value
    else:
        TRX = random.uniform(VTP_range[14, 0], TRX_right_boundary)

    random_params = np.array([
        HX, HY, JX, JA, LP, LD, VS, VO, 
        TCX, TCY, TTX, TTY, TBX, TBY, TRX, TRY, TS1, TS2, TS3
        ])
    return random_params


def randomVTP_C(
        consonantal_dimension_index_array,
        params_V,
        VTP_range
        ):
    params_C = np.copy(params_V)
    for cdi in consonantal_dimension_index_array:
        params_C[cdi] = random.uniform(
            VTP_range[cdi, 0], VTP_range[cdi, 1])
    return params_C

def randomGlottis(GLP_neutral, GLP_range):
    F0=GLP_neutral[0];
    Lung_pressure=random.uniform(GLP_range[1, 0],GLP_range[1, 1]);
    Upper_rest_displacement=random.uniform(GLP_range[2, 0],GLP_range[2, 1]);
    Lower_rest_displacement=random.uniform(GLP_range[3, 0],GLP_range[3, 1]);
    Arytenoid_area=GLP_neutral[4];
    Aspiration_strength=random.uniform(GLP_range[5, 0],GLP_range[5, 1]);
    random_params = np.array([F0,Lung_pressure,Upper_rest_displacement,
        Lower_rest_displacement,Arytenoid_area,Aspiration_strength]);
    return  random_params

def wav_normalise(wav, level=-1):
    """Normalise wav array.

    Parameters
    ----------
    wav : ndarray
        Input wave array.
    level : float
        Level in dB.
    """
    if np.max(np.abs(wav)) > 0:
        level = 10**(level/20)
        return wav / np.max(np.abs(wav)) * level
    else:
        return wav


def wav_fade(wav, n_fade=200):
    """Fade wav array.

    Parameters
    ----------
    wav : ndarray
        Input wave array.
    n_fade : int
        samples to fade in and out.
    """
    fade = np.linspace(0, 1, n_fade)  # linear fade - simpler
    # fade = np.exp((fade-1)/.2)  # exp fade - probably overkill
    wav[:n_fade] *= fade
    wav[-n_fade:] *= fade[::-1]
    return wav


def wav_write(wav, path, sr):
    """Writes wav file from float audio to path with f_s = sr
    """
    wav_int = np.int16(wav * (2**15 - 1))
    wavfile.write(path, sr, wav_int)


def calcParamCurve(parameter_dicts):
    """Calculate curve array by Target Approximation model
    """
    EPSILON = 0.000000001  # arbitrary small number
    CURVE_SAMPLING_RATE = 220  # constant 200 Hz = 5 ms point spacing
    MAX_CURVE_SAMPLES = CURVE_SAMPLING_RATE * 60
    # if len(parameter_list1) < 0:
    #     break
    numTargets = parameter_dicts.shape[0]
    targetIndex = 0
    targetPos_s = 0.0
    totalDuration = parameter_dicts["duration_s"].sum()
    finalPointIndex = int((totalDuration + 0.01) * CURVE_SAMPLING_RATE)
    if finalPointIndex >= MAX_CURVE_SAMPLES:
        finalPointIndex = MAX_CURVE_SAMPLES - 1
    paramCurve = np.zeros(finalPointIndex)  # empty array
    # first time constant
    if abs(parameter_dicts["tau_s"][targetIndex]) < EPSILON:
        parameter_dicts["tau_s"][targetIndex] = EPSILON
    a = -1.0 / parameter_dicts["tau_s"][targetIndex]
    a2 = a*a
    a3 = a2*a
    a4 = a3*a

    # Coefficients for the initial target are all zero
    # -> we follow the target exactly.
    c0 = 0.0
    c1 = 0.0
    c2 = 0.0
    c3 = 0.0
    c4 = 0.0

    for i in range(finalPointIndex):
        t_s = i / CURVE_SAMPLING_RATE  # sampling time point
        while ((t_s > targetPos_s + parameter_dicts["duration_s"][targetIndex])
               and (targetIndex < numTargets - 1)):
            # Calculate y(t) and its derivatives at the end of the prev target
            t = parameter_dicts["duration_s"][targetIndex]
            t2 = t*t
            t3 = t2*t
            t4 = t3*t
            # It's important to consider the slope for f1!
            f0 = (
                np.exp(a*t)*((c0)+(c1)*t + (c2)*t2 + (c3)*t3 + (c4)*t4)
                + (
                    parameter_dicts["dVal"][targetIndex]
                    + parameter_dicts["duration_s"][targetIndex]
                    * parameter_dicts["slope"][targetIndex]
                    )
                )
            f1 = (
                np.exp(a*t)*(
                    (c0*a + c1)
                    + (c1*a + 2 * c2)*t
                    + (c2*a + 3 * c3)*t2
                    + (c3*a + 4 * c4)*t3
                    + (c4*a)*t4
                    )
                + parameter_dicts["slope"][targetIndex]
                )
            f2 = (
                np.exp(a*t)*(
                    (c0*a2 + 2 * c1*a + 2 * c2)
                    + (c1*a2 + 4 * c2*a + 6 * c3)*t
                    + (c2*a2 + 6 * c3*a + 12 * c4)*t2
                    + (c3*a2 + 8 * c4*a)*t3
                    + (c4*a2)*t4
                    )
                )
            f3 = (
                np.exp(a*t)*(
                    (c0*a3 + 3 * c1*a2 + 6 * c2*a + 6 * c3)
                    + (c1*a3 + 6 * c2*a2 + 18 * c3*a + 24 * c4)*t
                    + (c2*a3 + 9 * c3*a2 + 36 * c4*a)*t2
                    + (c3*a3 + 12 * c4*a2)*t3
                    + (c4*a3)*t4
                    )
                )
            f4 = (
                np.exp(a*t)*(
                    (c0*a4 + 4 * c1*a3 + 12 * c2*a2 + 24 * c3*a + 24 * c4)
                    + (c1*a4 + 8 * c2*a3 + 36 * c3*a2 + 96 * c4*a)*t
                    + (c2*a4 + 12 * c3*a3 + 72 * c4*a2)*t2
                    + (c3*a4 + 16 * c4*a3)*t3 + (c4*a4)*t4
                    )
                )
            # Go to the next target.
            targetPos_s += parameter_dicts["duration_s"][targetIndex]
            targetIndex += 1
            # Calc. the coefficients for the next target based on the
            # derivatives at the end of the previous target.
            if parameter_dicts["tau_s"][targetIndex] < EPSILON:
                parameter_dicts["tau_s"][targetIndex] = EPSILON
            a = -1.0 / parameter_dicts["tau_s"][targetIndex]
            a2 = a*a
            a3 = a2*a
            a4 = a3*a

            c0 = f0 - parameter_dicts["dVal"][targetIndex]
            # Slope must be considered here!
            c1 = (f1 - c0*a - parameter_dicts["slope"][targetIndex])
            c2 = (f2 - c0*a2 - c1*a * 2) / 2
            c3 = (f3 - c0*a3 - c1*a2 * 3 - c2*a * 6) / 6
            c4 = (f4 - c0*a4 - c1*a3 * 4 - c2*a2 * 12 - c3*a * 24) / 24

        t = t_s - targetPos_s  # Time relative to the beginning of the target
        t2 = t*t
        t3 = t2*t
        t4 = t3*t
        paramCurve[i] = (
            np.exp(a*t)*(
                (c0)+(c1)*t
                + (c2)*t2
                + (c3)*t3
                + (c4)*t4
                )
            + (
                parameter_dicts["dVal"][targetIndex]
                + t*parameter_dicts["slope"][targetIndex]
                )
            )
    return paramCurve
